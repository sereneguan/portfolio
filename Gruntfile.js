module.exports = function ( grunt ) {
    require( 'matchdep' )
        .filterDev( 'grunt-*', './package.json' )
        .forEach( grunt.loadNpmTasks );

    grunt.initConfig( {
        pkg: grunt.file.readJSON( 'package.json' ),

        // Grunt Watch
        watch: {
            options: {
                reload: true,
                atBegin: true
            },
            css: {
                files: [
                    'sass/style.scss',
                    'sass/*/**/*.scss'
                ],
                tasks: [ 'sass:dev' ]
            }
        },

        // Task - Compile SASS
        sass: {
            options: {
                sourceMap: false
            },
            dev: {
                files: {
                    'css/style.css': 'sass/style.scss'
                }
            }
        },

        // Task - PostCSS
        postcss: {
            options: {
                map: false,
                processors: [
                    require( 'autoprefixer' )( {
                        browsers: [ 'last 4 versions', 'ie >= 9' ]
                    } )
                ]
            },
            dev: {
                src: 'css/style.css'
            }
        },


    } );

    grunt.registerTask( 'default', [] );
};
