<footer>
    <section id="contact" class="footersection1">
        <div class="footerwrapper">
            <div class="resume"><a target="_blank" href="Serene-Guan-Resume.pdf">Resume</a></div>

            <div class="underhireme">
                <div class="footerinformation">
                    <h3 class="footerh3">Serene Guan</h3>
                    <p>Cat&nbsp;&&nbsp;Rubic Cube Lover&nbsp;&hearts;</p>
                    <p>UX/UI Designer | Front-end Developer | Industrial Designer</p>
                    <br>
                    <br>
                    <h3 class="footerh3">Contact</h3>
                    <p>1(289)6810120</p>
                    <a href="mailto:sereneguan@gmail.com?Subject=Hello%20Serene" target="_top">
                        <p class="clickemail">sereneguan@gmail.com</p>
                    </a>

                    <?php get_template_part('templates/footer', 'social'); ?>

                </div>

                <?php get_template_part('templates/footer', 'contact'); ?>

            </div>

        </div>
    </section>
    <section class="footersection2">
        <div class="footerwrapper">
            <div class="footerbottom">
                <div class="footerlogo">
                    <?php echo file_get_contents(get_template_directory_uri().'/img/svg/footerlogo.svg'); ?>
                </div>
                <div>
                    <p class="copyrightp"> &copy; 2016 Serene Guan. All rights reserved.</p>
                </div>
            </div>
        </div>
    </section>
</footer>


</div>





<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/script-main.js"></script>
</body>

</html>
