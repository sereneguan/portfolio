<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<div class="blog-single">
    <div class="container">
        <h1>
            <?php the_title(); ?>
        </h1>
        <div class="post-date">
            Posted on
            <?php the_time('F j, Y'); ?>
        </div>
        <p>
            <?php the_content(); ?>
        </p>
    </div>
</div>

<?php                       
    endwhile;                        
    wp_reset_postdata()
?>

<?php get_footer(); ?>
