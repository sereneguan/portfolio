<?php
if ( ! function_exists( 'portfolio_setup' ) ) :

	function portfolio_setup() {
		// Register Nav Menus
		register_nav_menus( array(
			'main-nav'   => __( 'Main Navigation', 'portfoliotheme' ),
			'main-nav-home'   => __( 'Main Navigation (Home)', 'portfolitheme' )
		) );
        // Thumbnail
        add_theme_support( 'post-thumbnails' );
	}

endif;	// if ( function_exists( 'portfoli2015_setup') )

add_action( 'after_setup_theme', 'portfolio_setup' );

function my_custom_posttypes() {	

	// Projects CPT
    $labels = array(
        'name'               => 'Work',
        'singular_name'      => 'Work',
        'menu_name'          => 'Work',
        'name_admin_bar'     => 'Work',
        'add_new'            => 'Add New',
        'add_new_item'       => 'Add New Work',
        'new_item'           => 'New Work',
        'edit_item'          => 'Edit Work',
        'view_item'          => 'View Work',
        'all_items'          => 'All Work',
        'search_items'       => 'Search Work',
        'parent_item_colon'  => 'Parent Work',
        'not_found'          => 'No Work found.',
        'not_found_in_trash' => 'No Work found in Trash.'
    );
    
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'menu_icon'          => 'dashicons-portfolio',
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'work' ),
        'capability_type'    => 'post',        
        'hierarchical'       => false,
        'menu_position'      => 5,        
        'supports'           => array('title','editor','author','revisions','page-attributes', 'thumbnail')
    );
    register_post_type('work', $args); 

	flush_rewrite_rules();
}
add_action('init', 'my_custom_posttypes');

// Flush rewrite rules to add "review" as a permalink slug
function my_rewrite_flush() {
    my_custom_posttypes();
    flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'my_rewrite_flush' );

// Allow SVG Uploads
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  $mimes['svgz'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');




?>