<section id="engage" class="blogsection">
    <div class="container">
        <h1 class="engage">blog</h1>
        <div class="dotcontainer">
            <div class="engagedot"></div>
        </div>
        <div class="blogwrapper">
            <div class="blogposts">
                <?php
                        $args = array( 
                            'post_type' => 'post',                          
                            'posts_per_page' => 3
                        );
                        $posts = new WP_Query( $args );                                         
                        while ( $posts->have_posts() ) : $posts->the_post();                    
                    ?>
                    <div>
                        <h3 class="posttitle">
                            <a href="<?php the_permalink(); ?>">
                                <?php the_title(); ?>
                            </a>
                        </h3>
                        <span class="postdate"><?php the_time('F j, Y'); ?></span>
                        <p>
                            <?php the_excerpt(); ?>
                        </p>
                        <p><a href="<?php the_permalink(); ?>" class="continue">CONTINUE READING...</a></p>
                    </div>
                    <?php                       
                        endwhile;                        
                        wp_reset_postdata()
                    ?>

            </div>
        </div>
    </div>
</section>
