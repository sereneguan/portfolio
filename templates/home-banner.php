<section class="landingbody">
    <div class="logobg-big"></div>
    <div class="logobg-small"></div>
    <div class="logo-s">
        <?php echo file_get_contents(get_template_directory_uri().'/img/svg/landinglogo/logo_s.svg'); ?>
    </div>
    <div class="logo-innerdots">
        <?php echo file_get_contents(get_template_directory_uri().'/img/svg/landinglogo/logo_innerdots.svg'); ?>
    </div>

    <div class="logo-outterdots">
        <?php echo file_get_contents(get_template_directory_uri().'/img/svg/landinglogo/logo_outterdots.svg'); ?>
    </div>

    <div class="selfdescription">
        <span>Hi, I am&nbsp;&nbsp;<a href="https://twitter.com/Serene_Guan" target="_blank"><strong>@Serene_Guan</strong></a>&nbsp;&nbsp;:)</span>
        <br>
        <span>A UX/UI Designer <strong>&</strong> Front-end Developer <strong>&</strong> Industrial Designer</span>
        <div class="typing-slider">
            <p>I love all kitties and rubic cubes.</p>
            <p>I love creating something from nothing.</p>
            <p>I build cool websites.</p>
            <p>I make elegant melody by violin.</p>
        </div>
    </div>
    <div class="scrolldown">
        <div class="catpaws">
            <?php echo file_get_contents(get_template_directory_uri().'/img/svg/catfootprint.svg'); ?>
        </div>
    </div>
</section>
