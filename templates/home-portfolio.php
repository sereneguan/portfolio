<section id="work" class="portfolio">
    <h1 class="work">work</h1>


    <div class="workcoverwrapper">
        <ul id="filters" class="filters">
            <li id="filter-all" class="filter-all active" data-filter="*">ALL</li>
            <li id="filter-uiux" class="filter-uiux" data-filter=".uiuix">UI/UX</li>
            <li id="filter-dev" class="filter-dev" data-filter=".dev">DEV</li>
            <li id="filter-industrial" class="filter-industrial" data-filter=".industrial">INDUSTRIAL</li>
            <li id="filter-graphix" class="filter-graphix" data-filter=".graphix">GRAPHIX</li>
        </ul>

        <ul id="work-list" class="workcover">
            <?php 
                        $args = array( 
                            'post_type' => 'work',
                            'posts_per_page' => -1,
                            'orderby' => 'menu_order',
                            'order' => 'DESC'
                        );
                        $posts = new WP_Query( $args ); 
                        while ( $posts->have_posts() ) : $posts->the_post();                                                
                    ?>
        <li class="element-item <?php echo get_field('work_category'); ?>">
                <a href="<?php esc_url( the_permalink() ); ?>">
                    <?php the_post_thumbnail(); ?>
                </a>
            </li>
            <?php                            
                endwhile;
                wp_reset_postdata();
            ?>
        </ul>

    </div>
</section>
