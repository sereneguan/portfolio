﻿package fighting
{

	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.KeyboardEvent;


	public class Fighting extends MovieClip
	{
		private var toleft:Boolean;
		private var toright:Boolean;
		private var speed:int = 6;
		private var Garget:int = 6;
		private var upPower:int = 0;
		private var onground:MovieClip;
		private var bioarray:Array = [];
		private var jumpPower:int = 10;


		public function Fighting()
		{
			// constructor code


			getAldo();
			getinit();
			onground = null;

		}
		function getinit()
		{
			aldo.addEventListener(Event.ENTER_FRAME, getEvt);
		}
		function getAldo()
		{
			addEventListener(Event.ADDED_TO_STAGE, stageEvt);
		}
		function stageEvt(e:Event):void
		{
			stage.addEventListener(KeyboardEvent.KEY_DOWN, keydown);
			stage.addEventListener(KeyboardEvent.KEY_UP, keyup);
			addEventListener(Event.ENTER_FRAME, aldoEvt);
		}

		public function getEvt(e:Event):void
		{
			doFall();
			doMove();
			doJump();
		}
		public function doFall():void
		{
			aldo.y +=  Garget;
		}
		public function doMove():void
		{
			var leftdirect:Number;
			var rightdirect:Number;
			toleft ? leftdirect = -1:leftdirect = 0;
			toright ? rightdirect = 1:rightdirect = 0;
			aldo.x +=  speed * (leftdirect + rightdirect);

		}
		public function doJump():void
		{
			if (upPower > 0)
			{
				upPower--;
				aldo.y -=  Garget * 2;
			}
		}
		public function jumpValue(value:int):void
		{
			if (onground != null && onground is aldo.hitTestObject(onground))
			{
				upPower = value;
			}

		}
		public function onbaffes(srt:String):void
		{

		}
		public function doDie()
		{
			trace("die");
		}
		public function getDie():void
		{
			aldo.removeEventListener(Event.ENTER_FRAME, getEvt);
			doDie();
		}
		function aldoEvt(e:Event):void
		{
			if (aldo.x <= 10)
			{
				super.toleft = false;
				super.toright = true;
			}
			else
			if (aldo.x >= 540)
			{
				super.toleft = true;
				super.toright = false;
			}
		}
		function keydown(e:KeyboardEvent):void
		{
			if (e.keyCode == 37)
			{
				super.toleft = true;
				aldo.scaleX = -1;
				aldo.gotoAndStop("up");
			}
			if (e.keyCode == 39)
			{
				super.toright = true;
				aldo.scaleX = 1;
			    aldo.gotoAndStop("up");
			}
			if (e.keyCode == 32)
			{
				super.jumpValue(jumpPower);
			}
		}
		function keyup(e:KeyboardEvent):void
		{
			super.toleft = false;
			super.toright = false;
			aldo.gotoAndStop("down");
		}
		
	}
	
}


