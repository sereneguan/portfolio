<?php get_template_part('templates/home', 'banner'); ?>

<?php get_header(); ?>

<?php get_template_part('templates/home', 'portfolio'); ?>

<?php get_template_part('templates/home', 'about'); ?>

<?php get_template_part('templates/home', 'blog'); ?>

<?php get_footer(); ?>
