<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

    <div class="jumbotron">            
        <img src="<?php echo get_field('work_jumbotron_image'); ?>" alt="">
    </div>

    <div class="dotcontainer"><div class="workdot"></div></div>
    
    <h1 class="worktitle"><?php the_title(); ?></h1>
   
    <div class="row">
        <div class="left">
            <div class="leftcontent">
                <h2>Description</h2>
                    <div class="textbox"><?php the_content(); ?></div>
            </div>
        </div>
        <div class="right">
            <div class="rightcontent">
                <img src="<?php echo get_field('work_image_1'); ?>" alt="">        
            </div>    
        </div>
    </div>
    <div class="row">   
        <div class="left">
            <div class="leftcontent">
                <img src="<?php echo get_field('work_image_2'); ?>" alt="">
            </div>
        </div>
        <div class="right">
            <div class="rightcontent">
                <h2>Context and Challenge</h2>        
                <div class="textbox"><?php echo get_field('work_context_and_challenge'); ?></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="left">
            <div class="leftcontent">
                <h2>Deliverables</h2>
                <div class="textbox"><?php echo get_field('work_deliverables'); ?></div>
            </div>
        </div>
        <div class="right">
            <div class="rightcontent">
                <img src="<?php echo get_field('work_image_3'); ?>" alt="">
            </div>
        </div>
    </div>
    <div class="workcontact"><h2>Please feel free to contact Serene Guan for more details.</h2></div>
    <div class="scrolldown">
            <div class="catpaws"><?php echo file_get_contents(get_template_directory_uri().'/img/svg/catfootprint.svg'); ?></div>                    
    </div>

<?php endwhile; ?>

<?php get_footer(); ?>