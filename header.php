<!DOCTYPE html>
<html>

    <head>
        <title>Serene.Guan - UX/UI Designer | Front-end Developer | Industrial Designer</title>
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/img/bm/logo.ico" type="image/ico">
        <link rel="Bookmark" href="img/bm/logo.ico" />
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
      
    </head>

    <body>



        <div class="headertop"></div>
        <header class="homeheader <?php if( is_front_page() ) echo 'header-home'; ?>">
            <div class="header-logo-container">
                <div class="headerlogo">
                    <a href="/">
                        <?php echo file_get_contents(get_template_directory_uri().'/img/svg/landinglogo/logo_header.svg'); ?>
                    </a>
                </div>
                <nav class="nav">
                    <?php
				wp_nav_menu( array( 
					'theme_location' => 'main-nav-home',
					'menu_class' => '' 
					) 
				); 						
			?>
                    
                </nav>

                <!--<div class="burger-menu">
                        <?php echo file_get_contents(get_template_directory_uri().'/img/svg/menu.svg'); ?>
                </div>-->
            </div>
        </header>
